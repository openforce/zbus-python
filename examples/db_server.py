#encoding=utf8  
import sys   
sys.path.append("../") 

from sqlalchemy import create_engine, select
from zbus import Application 
  
from zbus.db import Db 
engine = create_engine("mysql+pymysql://root:root@localhost/buildmaster?charset=utf8", pool_size=64, echo=False)
db = Db(engine)


class DbService:
    def __init__(self, db):
        self.db = db
    
    def user_list(self):
        return self.db.query_page('select * from _user')

    def user(self):
        return self.db.query_one('select * from _user limit 1')

    def test(self):
        return 'ok'
    
app = Application() 
app.mount('/', DbService(db))
app.mount('/', db)
app.run(url='localhost:15555')
