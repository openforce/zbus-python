#encoding=utf8    

import sys
sys.path.append("../")  

from zbus import Application, route

class MyService:  
    def echo(self, ping):
        return str(ping)
     
    def plus(self, a, b):  
        return int(a) + int(b) 
    
    @route("/")
    def home(self):  
        return u'中文' 
     


app = Application() 
app.mount('/', MyService()) #relative mount url  

app.run(url='zbus.io/example',
        api_key = '2ba912a8-4a8d-49d2-1a22-198fd285cb06',
        secret_key = '461277322-943d-4b2f-b9b6-3f860d746ffd',
        auth_enabled = False) 